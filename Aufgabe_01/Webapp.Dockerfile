# Von welchem offiziellen Paket wird das Base-Image bezogen
# Ggf. ubuntu-slim oder alpine
FROM ubuntu:latest
# Installation von weiteren Paketen & update der Paketquellen
RUN apt-get update && apt-get install -y curl apache2
# Kopieren von z.B. Skripten in den Container
COPY ./index.html /var/www/html
# Setzt Arbeitsverzeichnis
# WORKDIR /var/www/html
# /var/www/html/js/src 
# COPY src/ /js/src/

# Freigabe des Ports für apache
EXPOSE 80
# Einstiegspunkt - Apache starten
#ENTRYPOINT [ "/bin/bash" "top"]
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]